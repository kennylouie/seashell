#include <stdlib.h>
#include <stdio.h>
#include <string.h> // strtok
#include <unistd.h> // fork, exec, pid_t
#include <sys/wait.h> // waitpid

#include "seashell.h"

char* sea_read_line(void);
char** sea_split_line(char*);
int sea_exec(char**);

/* runs the main functions in an infinite loop */
void
sea_loop(void)
{
	char* line;
	char** args;
	int status;

	do
	{
		printf("C/?> ");

		line = sea_read_line();

		args = sea_split_line(line);

		status = sea_exec(args);

		free(line);

		free(args);
	} while (status);
}

/* reads in stdin
 * similar to getline() */
#define LINE_BUFFER_SIZE 1024

char*
sea_read_line(void)
{
	int buffer_size = LINE_BUFFER_SIZE;

	int position = 0;

	char* buffer = malloc(sizeof(char) * buffer_size);

	int ch;

	if (!buffer)
	{
		fprintf(stderr, "seashell: could not allocate memory error\n");
		exit(EXIT_FAILURE);
	}

	while(1)
	{
		ch = getchar();

		/* when we get to end of file, replcace with null character and return */
		if (ch == EOF || ch == '\n')
		{
			buffer[position] = '\0';

			return buffer;
		}
		else
		{
			buffer[position] = ch;
		}

		position++;

		/* if buffer exceeded, reallocate */
		if (position >= buffer_size)
		{
			buffer_size += LINE_BUFFER_SIZE;

			buffer = realloc(buffer, buffer_size);

			if (!buffer)
			{
				fprintf(stderr, "seashell: could not allocate memory error\n");
				exit(EXIT_FAILURE);
			}
		}
	}
}

/* splits the line by deliminiters */
#define TOKEN_BUFFER_SIZE 64
#define TOKEN_DELIMS " \t\r\n\a"

char**
sea_split_line(char* line)
{
	int buffer_size = TOKEN_BUFFER_SIZE, position = 0;

	char** tokens = malloc(sizeof(char*) * buffer_size);

	char* token;

	if (!tokens)
	{
		fprintf(stderr, "seashell: could not allocate memory error\n");
		exit(EXIT_FAILURE);
	}

	token = strtok(line, TOKEN_DELIMS);

	while(token != NULL)
	{
		tokens[position] = token;

		position++;

		if (position >= buffer_size)
		{
			buffer_size += TOKEN_BUFFER_SIZE;
			tokens = realloc(tokens, buffer_size * sizeof(char));
			if (!tokens)
			{
				fprintf(stderr, "seashell: could not allocate memory error\n");
				exit(EXIT_FAILURE);
			}
		}

		token = strtok(NULL, TOKEN_DELIMS);
	}

	tokens[position] = NULL;

	return tokens;
}

/* unix exec the commands */
int
sea_exec(char** args)
{
	pid_t pid;
	int status;

	pid = fork();
	
	if (pid == 0)
	{
		/* child process has pid 0 */
		if (execvp(args[0], args) == -1)
			perror("seashell");

		/* exit the process */
		exit(EXIT_FAILURE);
	}
	/* error forking */
	else if (pid < 0)
		perror("seashell");
	else
	{
		/* parent process */
		do
		{
			waitpid(pid, &status, WUNTRACED);
		} while( !WIFEXITED(status) && !WIFSIGNALED(status) );
	}

	/* returns a non zero value to keep shell loop */
	return EXIT_FAILURE;
}
